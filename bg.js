const body = document.querySelector('body');

const IMG_LENGTH = 3; 

function getRandom() { 
    const number = Math.floor(Math.random() * IMG_LENGTH);
    return number;
}

function init() {
    const randomNumber = getRandom();
    //console.log(typeof(randomNumber))
    body.style.backgroundImage = `url(images/${randomNumber + 1}.jpg)`;
}

init();