const container = document.querySelector('.clock_container'),
    clockTitle = container.querySelector('time');

function getTime() { 
    const date = new Date();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    if (seconds < 10) { 
        //return '0'+seconds;
    } 
    clockTitle.innerText = `${hours < 10 ? `0${hours}` : `${hours}`}:${minutes < 10 ? `0${minutes}` : `${minutes}`}:${seconds < 10 ? `0${seconds}` : `${seconds}`}`;
     

}
function init() { 
    setInterval( () => {
        getTime();
    }, 1000);
    
}


init(); 