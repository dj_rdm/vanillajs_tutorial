const form = document.querySelector('.js-form'),
    input = form.querySelector('input'),
    greeting = document.querySelector('.greeting');

const USER_LS = "name";


function askName() { 
    form.style.display = 'block';
    form.addEventListener('submit', addName);
};

function saveName(text) { 
    localStorage.setItem(USER_LS, text);
}

function addName(event) { 
    event.preventDefault();
    highlightName(input.value);    
    saveName(input.value);
}



function highlightName(text) { 
    greeting.style.display = 'block';
    form.style.display = 'none';
    greeting.innerText = `Hello ${text}`;
    greeting.style.color = 'red';
}
function loadName() { 
    const currentUser = localStorage.getItem(USER_LS);

    if (currentUser === null) {
        //greeting.style.display = 'block';
        //input.style.display = 'block';
        askName();
    }
    else { 
        //localStorage.setItem(USER_LS, input.value)
        highlightName(currentUser);
    }
}

function init() {
    loadName();
    //setName();

}


init(); 