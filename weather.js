const weather = document.querySelector('.js-weather');

const COORDS = 'coords';
const API_KEY = "3f9404e51a676528522e910eefbc4158";

function getWeather(lat, long) { 
    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${API_KEY}&units=metric`).then(function (response) { 
        return response.json();
    }).then(function (json) { 
        console.log(json);
        const temp = json.main.temp;
        const place = json.name;
        const weatherStatus = json.weather[0].main;
        weather.innerText = `temp: ${temp} / place: ${place} / status = ${weatherStatus}`
    });
}

function saveCoords(coordsObj) {
    localStorage.setItem(COORDS, JSON.stringify(coordsObj));
}
function successCoords(position) { 
    //console.log(position);
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const coordsObj = {
        latitude,
        longitude
    }
    saveCoords(coordsObj);
    getWeather(latitude, longitude);
}



function errorCoords() { 
    console.log('error call coords!')
}

function askForCoords() { 
    navigator.geolocation.getCurrentPosition(successCoords, errorCoords);
}

function loadCoords() { 
    const loadedCoords = localStorage.getItem(COORDS);
    if (loadedCoords === null) {
        askForCoords();
    }
    else { 
        const parsedCoords = JSON.parse(loadedCoords);
        getWeather(parsedCoords.latitude, parsedCoords.longitude)
    }
}


function init() { 
    loadCoords();
}

init();